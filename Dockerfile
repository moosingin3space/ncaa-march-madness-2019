FROM registry.fedoraproject.org/fedora:29

RUN INSTALL_PKGS="ruby ruby-devel rubygem-bundler make gcc redhat-rpm-config glibc-devel" && \
    dnf install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    dnf clean all

WORKDIR /app
