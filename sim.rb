require 'optparse'
require 'yaml'
require 'pry'
require 'deep_clone'
require 'solid_assert'
require 'pp'
require_relative './refinements.rb'
require_relative './models.rb'

REGIONS = [:east, :west, :midwest, :south]
INITIAL_GAMES = [
  [1, 16], [8, 9], [5, 12], [4, 13], [6, 11], [3, 14], [7, 10], [2, 15]
]
REGION_PROGRESSION = [
  [[1, 16], [8, 9], [5, 12], [4, 13], [6, 11], [3, 14], [7, 10], [2, 15]],
  [[], [], [], []],
  [[], []],
  [[]],
  [[]]
]

def normalize_stats(stats)
  assert stats.has_key? :offense
  assert stats[:offense].has_key? :fg
  assert stats[:offense].has_key? :rb
  assert stats.has_key? :defense
  assert stats[:defense].has_key? :fg
  assert stats[:defense].has_key? :rb
  assert stats.has_key? :games
  {
    offense: {
      fg: stats[:offense][:fg],
      rb: stats[:offense][:rb]/stats[:games]
    },
    defense: {
      fg: stats[:defense][:fg],
      rb: stats[:defense][:rb]/stats[:games]
    }
  }
end

def do_round(model_proc, games, &result_func)
  games.each do |game|
    winner_idx, margin = model_proc.call(game[:teams])
    loser_idx = 1 - winner_idx
    result_func.call(game[:teams][winner_idx], game[:teams][loser_idx], game[:region], margin)
  end
end

model_func = nil
OptionParser.new do |opts|
  opts.banner = 'Usage: sim.rb [-2|-4]'

  opts.on('-2', '--two-dimensional', 'Two-dimensional model') do |v|
    model_func = Proc.new { |teams| Model.two(teams) }
  end
  opts.on('-4', '--four-dimensional', 'Four-dimensional model') do |v|
    model_func = Proc.new { |teams| Model.four(teams) }
  end
  opts.on('-s', '--simple', 'Simple model') do |v|
    model_func = Proc.new { |teams| Model.simple(teams) }
  end
end.parse!

if model_func.nil?
  STDERR.puts 'Please specify a model!'
  exit
end

tourney_defs = SymbolizeHelper.symbolize_recursive(YAML.load_file('teams.yaml'))

tourney_teams = {
  east: {},
  west: {},
  midwest: {},
  south: {}
}

tourney_progression = {
  east: DeepClone.clone(REGION_PROGRESSION),
  west: DeepClone.clone(REGION_PROGRESSION),
  midwest: DeepClone.clone(REGION_PROGRESSION),
  south: DeepClone.clone(REGION_PROGRESSION),
}

# Assign all teams to the tourney by rank
REGIONS.each do |region|
  tourney_defs[region][:teams].each do |name, stats|
    if stats[:rank] != :"play-in"
      tourney_teams[region][stats[:rank]] = {
        name: name,
        rank: stats[:rank],
        stats: normalize_stats(stats)
      }
    end
  end
end

# Play-in round
games = REGIONS.map do |region|
  games = tourney_defs[region][:"play-in"]
  games.map do |game|
    {
      teams: game[:teams].map do |team|
        {
          name: team,
          rank: game[:rank],
          games: game[:games],
          stats: normalize_stats(tourney_defs[region][:teams][team])
        }
      end,
      region: region,
    }
  end
end.flatten

do_round(model_func, games) do |winner, loser, region, margin|
  puts "Team #{winner[:name].to_s} def. #{loser[:name].to_s} by #{margin}"
  tourney_teams[region][winner[:rank]] = winner
end

# Regional tournaments
REGIONS.each do |region|
  (0..3).each do |round_num|
    games = tourney_progression[region][round_num].each_with_index.map do |game, i|
      {
        teams: game.map do |rank|
          team = tourney_teams[region][rank] 
          team[:game_index] = i
          team
        end,
        region: region
      }
    end

    do_round(model_func, games) do |winner, loser, region, margin|
      puts "Team #{winner[:name].to_s} def. #{loser[:name].to_s} by #{margin}"
      tourney_teams[region].delete(loser[:rank])
      new_game_index = winner[:game_index] / 2
      tourney_progression[region][round_num+1][new_game_index].push(winner[:rank])
    end
  end

  puts "The #{region} region played out like so:"
  pp tourney_progression[region]
  puts
end

# the Final Four
puts 'for the Final Four'
east_champ = tourney_progression[:east].last.last.last
west_champ = tourney_progression[:west].last.last.last
midwest_champ = tourney_progression[:midwest].last.last.last
south_champ = tourney_progression[:south].last.last.last
games = [
  {
    teams: [tourney_teams[:east][east_champ], tourney_teams[:south][south_champ]],
    region: :east_vs_south
  },
  {
    teams: [tourney_teams[:west][west_champ], tourney_teams[:midwest][midwest_champ]],
    region: :west_vs_midwest
  },
]
final_teams = []
do_round(model_func, games) do |winner, loser, region, margin|
  puts "Team #{winner[:name].to_s} def. #{loser[:name].to_s} by #{margin}"
  final_teams.push(winner)
end

puts "For the title: #{final_teams[0][:name]} vs #{final_teams[1][:name]}"
championship = {
  teams: final_teams,
  region: :national
}
do_round(model_func, [championship]) do |winner, loser, region, margin|
  puts "Team #{winner[:name].to_s} def. #{loser[:name].to_s} by #{margin} for the NATTY"
end
