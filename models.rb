module Model
  def Model.two(teams)
    x1 = teams[0][:stats][:offense][:fg]
    y1 = teams[0][:stats][:offense][:rb] + teams[0][:stats][:defense][:rb]
    m1 = x1*x1 + y1*y1

    x2 = teams[1][:stats][:offense][:fg]*100
    y2 = teams[1][:stats][:offense][:rb] + teams[1][:stats][:defense][:rb]
    m2 = x2*x2 + y2*y2

    dx = x1-x2
    dy = y1-y2
    margin = Math.sqrt(dx*dx + dy*dy)

    if m1 > m2
      return 0, margin
    else
      return 1, margin
    end
  end

  def Model.four(teams)
    x1 = teams[0][:stats][:offense][:fg]*100
    y1 = teams[0][:stats][:offense][:rb]
    z1 = (1 - teams[0][:stats][:defense][:fg])*100
    w1 = teams[0][:stats][:defense][:rb]
    m1 = x1*x1 + y1*y1 + z1*z1 + w1*w1

    x2 = teams[1][:stats][:offense][:fg]*100
    y2 = teams[1][:stats][:offense][:rb]
    z2 = (1 - teams[1][:stats][:defense][:fg])*100
    w2 = teams[1][:stats][:defense][:rb]
    m2 = x2*x2 + y2*y2 + z2*z2 + w2*w2

    dx = x1-x2
    dy = y1-y2
    dz = z1-z2
    dw = w1-w2
    
    margin = Math.sqrt(dx*dx + dy*dy + dz*dz + dw*dw)

    if m1 > m2
      return 0, margin
    else
      return 1, margin
    end
  end

  def Model.simple(teams)
    oa = teams[0][:stats][:offense]
    da = teams[0][:stats][:defense]

    ob = teams[1][:stats][:offense]
    db = teams[1][:stats][:defense]

    a_rank_delta = -(teams[0][:rank] - teams[1][:rank])/16.0

    a = (oa[:fg] + db[:fg])/2.0 + (1 - oa[:fg])*(oa[:rb]/db[:rb]) + (a_rank_delta*0.01)
    b = (ob[:fg] + da[:fg])/2.0 + (1 - ob[:fg])*(ob[:rb]/da[:rb])

    if a > b
      return 0, a - b
    else
      return 1, b - a
    end
  end
end
